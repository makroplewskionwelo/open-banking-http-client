package com.ndgit.openbanking.client.utils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import feign.Response;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;

import java.io.IOException;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

public class MappingFeignJacksonUtil {
    private static ObjectMapper jsonMapper = new ObjectMapper();

    public static <T> T mapResponseTo(Response response, Class<T> target) throws IOException {
        return jsonMapper.readValue(response.body().asInputStream(), target);
    }

    public static JacksonEncoder getJacksonEncoder() {
        ObjectMapper encoder = getBaseMapper();
        encoder.registerModule(getOffsetDataTimeToIsoInstantSerializer());
        return new JacksonEncoder(encoder);
    }

    public static JacksonDecoder getJacksonDecoder() {
        return new JacksonDecoder(getBaseMapper());
    }

    private static ObjectMapper getBaseMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        return objectMapper;
    }

    private static SimpleModule getOffsetDataTimeToIsoInstantSerializer() {
        SimpleModule offsetDateTimeSerializerModule = new SimpleModule();
        offsetDateTimeSerializerModule.addSerializer(OffsetDateTime.class, new JsonSerializer<OffsetDateTime>() {
            @Override
            public void serialize(OffsetDateTime offsetDateTime, JsonGenerator jsonGenerator,
                                  SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
                jsonGenerator.writeString(DateTimeFormatter.ISO_INSTANT.format(offsetDateTime));
            }
        });
        return offsetDateTimeSerializerModule;
    }
}
