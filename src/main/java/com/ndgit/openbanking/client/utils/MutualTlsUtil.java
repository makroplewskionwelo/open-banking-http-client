package com.ndgit.openbanking.client.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509KeyManager;
import java.io.File;
import java.io.FileInputStream;
import java.net.Socket;
import java.security.KeyStore;
import java.security.Principal;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;

/**
 * A util to generate Apache Http Clients which are able to use Mutual TLS.
 * To use Mutual TLS it requires a store for its own key as well as for the servers certificate.
 *
 * @author Mathias Bräu, Frank Belter, Moritz Wegmann
 */
@Slf4j
public class MutualTlsUtil {

    public static HttpClient getClientUsingMutualTLS(String keyStoreLocation, String trustStoreLocation,
                                                     String keyPassword, String trustPassword, String alias) {
        CloseableHttpClient client = null;
        try {
            HostnameVerifier hostnameVerifier = getHostNameVerifier();

            SSLSocketFactory sslSocketFactory = getSslSocketFactory(keyStoreLocation, trustStoreLocation, keyPassword,
                    trustPassword, alias);

            SSLConnectionSocketFactory sslConnectionSocketFactory = new SSLConnectionSocketFactory(sslSocketFactory,
                    hostnameVerifier);

            client = HttpClients.custom()
                    .setSSLSocketFactory(sslConnectionSocketFactory)
                    .build();

        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
        return client;
    }

    private static HostnameVerifier getHostNameVerifier() {
        return new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession sslSession) {
                return hostname.equals(sslSession.getPeerHost());
            }
        };
    }

    private static SSLSocketFactory getSslSocketFactory(String keyStoreLocation, String trustStoreLocation,
                                                        String keyPassword, String trustPassword, String alias)
            throws Exception {
        final KeyManagerFactory kmFactory = getKeyManagerFactory(keyStoreLocation, keyPassword, alias);
        final TrustManagerFactory tmFactory = getTrustManagerFactory(trustStoreLocation, trustPassword);

        SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
        sslContext.init(kmFactory.getKeyManagers(), tmFactory.getTrustManagers(), null);

        return sslContext.getSocketFactory();
    }

    private static TrustManagerFactory getTrustManagerFactory(String trustStoreLocation, String trustPassword)
            throws Exception {
        char[] tsPassword = trustPassword.toCharArray();
        KeyStore trustStore = getKeyStore(trustStoreLocation, tsPassword);

        final TrustManagerFactory tmFactory = TrustManagerFactory
                .getInstance(TrustManagerFactory.getDefaultAlgorithm());
        tmFactory.init(trustStore);

        return tmFactory;
    }

    private static KeyStore getKeyStore(String keyStoreLocation, char[] password) throws Exception {
        KeyStore keyStore = KeyStore.getInstance("jks");
        FileInputStream identityKeyStoreFile = new FileInputStream(new File(keyStoreLocation));
        keyStore.load(identityKeyStoreFile, password);
        return keyStore;
    }

    private static KeyManagerFactory getKeyManagerFactory(String keyStoreLocation, String keyPassword, String alias)
            throws Exception {
        char[] ksPassword = keyPassword.toCharArray();
        KeyStore keyStore = getKeyStore(keyStoreLocation, ksPassword);

        final KeyManagerFactory kmFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        kmFactory.init(keyStore, ksPassword);
        final X509KeyManager origKm = (X509KeyManager) kmFactory.getKeyManagers()[0];
        kmFactory.getKeyManagers()[0] = new AliasUsingKeyManager(origKm, alias);

        return kmFactory;
    }

    private static class AliasUsingKeyManager implements X509KeyManager {

        private final X509KeyManager originatingKeyManager;
        private final String alias;

        public AliasUsingKeyManager(X509KeyManager originatingKeyManager, String alias) {
            this.originatingKeyManager = originatingKeyManager;
            this.alias = alias;
        }

        public X509Certificate[] getCertificateChain(String alias) {
            return originatingKeyManager.getCertificateChain(alias);
        }

        public String[] getClientAliases(String keyType, Principal[] issuers) {
            return originatingKeyManager.getClientAliases(keyType, issuers);
        }

        public String chooseClientAlias(String[] keyType, Principal[] issuers, Socket socket) {
            return alias;
        }

        @Override
        public String chooseServerAlias(String keyType, Principal[] issuers, Socket socket) {
            return originatingKeyManager.chooseServerAlias(keyType, issuers, socket);
        }

        @Override
        public String[] getServerAliases(String keyType, Principal[] issuers) {
            return originatingKeyManager.getServerAliases(keyType, issuers);
        }

        @Override
        public PrivateKey getPrivateKey(String alias) {
            return originatingKeyManager.getPrivateKey(alias);
        }
    }
}
