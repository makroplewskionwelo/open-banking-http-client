package com.ndgit.openbanking.client;

import com.ndgit.openbanking.client.utils.MappingFeignJacksonUtil;
import com.ndgit.openbanking.client.utils.MutualTlsUtil;
import feign.Client;
import feign.Feign;
import feign.codec.ErrorDecoder;
import feign.httpclient.ApacheHttpClient;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.HttpClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class MutualTlsClient implements HttpFeignClient{

    @Value("${feign.client.baseUrl}")
    String baseUrl;

    @Value("${feign.ssl.keyStore}")
    String keyStoreLocation;

    @Value("${feign.ssl.trustStore}")
    String trustStoreLocation;

    @Value("${feign.ssl.keyPass}")
    String keyPassword;

    @Value("${feign.ssl.trustPass}")
    String trustPassword;

    @Value("${feign.ssl.alias}")
    String alias;

    public <T> T generateClientFor(Class<T> clientSpec, ErrorDecoder errorDecoder) {
        return getFeignBuilder(errorDecoder).target(clientSpec, baseUrl);
    }

    <T> Feign.Builder getFeignBuilder(ErrorDecoder errorDecoder) {
        JacksonDecoder jacksonDecoder = MappingFeignJacksonUtil.getJacksonDecoder();
        JacksonEncoder jacksonEncoder = MappingFeignJacksonUtil.getJacksonEncoder();

        HttpClient client = MutualTlsUtil.getClientUsingMutualTLS(keyStoreLocation, trustStoreLocation, keyPassword,
                trustPassword, alias);
        Client feignClient = new ApacheHttpClient(client);

        return Feign.builder()
                .client(feignClient)
                .errorDecoder(errorDecoder)
                .encoder(jacksonEncoder)
                .decoder(jacksonDecoder);
    }

    public void test(){
        log.info("MutualTls client");
    }
}
