package com.ndgit.openbanking.client;

import com.ndgit.openbanking.client.utils.MappingFeignJacksonUtil;
import feign.Feign;
import feign.codec.ErrorDecoder;
import feign.httpclient.ApacheHttpClient;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class CustomClient {

    @Value("${feign.client.baseUrl}")
    String baseUrl;

    public <T> T generateClientFor(Class<T> clientSpec, ErrorDecoder errorDecoder) {
        return getFeignBuilder(errorDecoder).target(clientSpec, baseUrl);
    }

    <T> Feign.Builder getFeignBuilder(ErrorDecoder errorDecoder) {
        JacksonDecoder jacksonDecoder = MappingFeignJacksonUtil.getJacksonDecoder();
        JacksonEncoder jacksonEncoder = MappingFeignJacksonUtil.getJacksonEncoder();

        return Feign.builder()
                .client(new ApacheHttpClient(HttpClients.custom().build()))
                .errorDecoder(errorDecoder)
                .encoder(jacksonEncoder)
                .decoder(jacksonDecoder);
    }
}
