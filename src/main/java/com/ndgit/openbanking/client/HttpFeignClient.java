package com.ndgit.openbanking.client;

import feign.codec.ErrorDecoder;

public interface HttpFeignClient {
    <T> T generateClientFor(Class<T> clientSpec, ErrorDecoder errorDecoder);
}
